Source: session-token
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libsession-token-perl <!nocheck>,
                     perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/session-token
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/session-token.git
Homepage: https://metacpan.org/release/App-Session-Token
Rules-Requires-Root: no

Package: session-token
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libsession-token-perl
Description: command-line script for generating session tokens
 session-token is a script, using the Session::Token module, to create session
 tokens, password reset codes, temporary passwords, random identifiers, etc.
 .
 It performs a similar task as `openssl rand -base64 16' but is more flexible
 regarding the alphabet used, and can efficiently generate a large number of
 random tokens.
